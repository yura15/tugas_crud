<html>
<title>TUGAS CRUD</title>
<head>
<link rel="stylesheet" href="style/materialize.min.css" />
</head>
<body>
<nav>
        <div class="nav-wrapper teal">
        <div class="container">
          <a href="index.php" class="brand-logo center white-text">WEBSITE DOSEN</a>
        </div>
        </div>
</nav>

<div class="container" style="margin-top:8%">
	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<p>
				<center>
					<h5 class= "cyan white-text">Data Jadwal Kelas</h5><hr>
				</center>
			</p>
			<br>
			<p>
				<a class="btn blue" href="dosen.php">Edit Data Dosen</a>
                <a class="btn green" href="kelas.php">Edit Data Kelas</a>
                <a class="btn yellow" href="jadwal.php">Edit Data Jadwal Kelas</a>
			</p>
			<table class="table table-bordered">
				<tr>
					<th>
						No 
					</th>
					<th>
						Nama Dosen
					</th>
                    <th>
						Jadwal
					</th>
                    <th>
						Mata Kuliah
					</th>
					<th>
						Kelas
					</th>
					<th>
						Prodi
					</th>
					<th>
						Fakultas
					</th>
				</tr>
					<?php
						include"koneksi.php";
						$no = 1;
                        $sql= "SELECT * FROM dosen INNER JOIN jadwal_kelas ON dosen.id_dosen=jadwal_kelas.id_dosen 
						INNER JOIN kelas ON kelas.id_kelas=jadwal_kelas.id_kelas";
						$data = mysqli_query ($koneksi, $sql);
						while ($row = mysqli_fetch_array ($data)){
                    ?>
                     <tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<?php echo $row['nama_dosen']; ?>
					</td>
					<td>
						<?php echo $row['jadwal']; ?>
					</td>
					<td>
						<?php echo $row['matakuliah']; ?>
					</td>
					<td>
						<?php echo $row['nama_kelas']; ?>
					</td>
                    <td>
						<?php echo $row['prodi']; ?>
					</td>
					<td>
						<?php echo $row['fakultas']; ?>
					</td>
				</tr>   
				
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>