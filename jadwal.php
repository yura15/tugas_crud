<html>
<title>TUGAS CRUD</title>
<head>
<link rel="stylesheet" href="style/materialize.min.css" />
</head>
<body>
<nav>
		<div class="nav-wrapper teal">
        <div class="container">
          <a href="index.php" class="brand-logo center white-text">CRUD</a>
        </div>
        </div>
</nav>
<div class="container" style="margin-top:8%">
	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			<p>
				<center>
					<h5 class="red white-text">Jadwal Kelas</h5><hr>
				</center>
			</p>
			<br>
			<p>
				<a class="btn btn-primary" href="tambah_jadwal.php">Tambah</a>
			</p>
			<table class="table table-bordered">
				<tr>
					<th>
						No 
					</th>
					<th>
						ID Dosen
					</th>
					<th>
						ID Kelas
					</th>
					<th>
						Jadwal
					</th>
                    <th>
						Matakuliah
					</th>
				</tr>
					<?php
						include"koneksi.php";
						$no = 1;
						$data = mysqli_query ($koneksi, " select 
																id_jadwal,
																id_dosen,
																id_kelas,
																jadwal,
                                                                matakuliah
														  from 
														  jadwal_kelas
														  order by id_jadwal DESC");
						while ($row = mysqli_fetch_array ($data))
						{
					?>
				<tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<?php echo $row['id_dosen']; ?>
					</td>
					<td>
						<?php echo $row['id_kelas']; ?>
					</td>
					<td>
						<?php echo $row['jadwal']; ?>
					</td>
                    <td>
						<?php echo $row['matakuliah']; ?>
					</td>
					<td>
						<a class="btn green" href="edit_jadwal.php?id=<?php echo $row['id_jadwal']; ?>">Edit</a> 
						<a class="btn red" href="hapus_jadwal.php?id=<?php echo $row['id_jadwal']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>